extends Control

onready var window = $"/root/Title/CanvasLayer/UI/Objektoj/Window"
onready var margin = $"/root/Title/CanvasLayer/UI/Objektoj/Window/VBox"

var camera #камера
export var z_away = 500 # насколько глубоко\далеко будет точка пути от экрана

var index_pos = 0


func _input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("left_click"):
		if event is InputEventMouseButton and event.doubleclick:
			#Если кнопка нажата, то бросаем луч из камеры на глубину z_away и получаем точку, тут же устанавливаем новый вейпойнт с координатами
			var celo = Transform(Basis.IDENTITY, camera.project_position(get_viewport().get_mouse_position(),z_away))
			Global.fenestro_itinero.okazigi_itinero(
				'', '', celo.origin.x, celo.origin.y, celo.origin.z, 
				celo, -1, Net.kategorio_movado, true
			)

	# проверяем, если открыто PopupMenu, а нажали в сторону
	if Input.is_action_just_pressed("left_click") or\
			Input.is_action_just_pressed("right_click"):
		var x = $".".get_global_mouse_position().x
		var y = $".".get_global_mouse_position().y
		if $canvas/PopupMenu.visible and not(($canvas/PopupMenu.margin_top<y) and \
					(y<($canvas/PopupMenu.margin_bottom+$canvas/PopupMenu.margin_top)) and \
					($canvas/PopupMenu.margin_left<x) and \
					(x<($canvas/PopupMenu.margin_right+$canvas/PopupMenu.margin_left))):
#			print('===$canvas/PopupMenu.margin_top=',$canvas/PopupMenu.margin_top)
#			print('===$canvas/PopupMenu.margin_bottom=',$canvas/PopupMenu.margin_bottom)
#			print('===$canvas/PopupMenu.margin_left=',$canvas/PopupMenu.margin_left)
#			print('===$canvas/PopupMenu.margin_right=',$canvas/PopupMenu.margin_right)
#			print('===закрываем окно y=',y,'; x=',x)
			$canvas/PopupMenu.visible=false
	# открываем PopupMenu
	if Input.is_action_just_pressed("right_click"):
#		event.pressed=true
		if event is InputEventMouseButton and $"/root/Title/CanvasLayer/UI/Objektoj/Window/VBox".visible:
			$canvas/PopupMenu.set_item_disabled(2,true)
			var x = $".".get_global_mouse_position().x
			var y = $".".get_global_mouse_position().y
			if (margin.margin_top+window.rect_global_position.y<y) and \
					(y<margin.margin_bottom+window.rect_global_position.y) and \
					(margin.margin_left+window.rect_global_position.x<x) and \
					(x<margin.margin_right+window.rect_global_position.x):
				$canvas/PopupMenu.margin_left=x
				$canvas/PopupMenu.margin_top=y
				$canvas/PopupMenu.visible=true
#				#если пункт меню - станция
				x = $"/root/Title/CanvasLayer/UI/Objektoj/Window/VBox/body_texture/ItemList".get_local_mouse_position().x
				y = $"/root/Title/CanvasLayer/UI/Objektoj/Window/VBox/body_texture/ItemList".get_local_mouse_position().y
				index_pos = $"/root/Title/CanvasLayer/UI/Objektoj/Window/VBox/body_texture/ItemList".get_item_at_position(Vector2(x,y),true)
				$"/root/Title/CanvasLayer/UI/Objektoj/Window/VBox/body_texture/ItemList".select(index_pos)
				if Global.objektoj[index_pos]['resurso']['objId'] == 1:#объект станция Espero
					#проверяем как далеко от станции и если менее 20, то разрешаем войти
					var dist = $"../ship".translation.distance_to(Vector3(
						Global.objektoj[index_pos]['koordinatoX'],
						Global.objektoj[index_pos]['koordinatoY'],
						Global.objektoj[index_pos]['koordinatoZ']
					))
					if dist<400:
						$canvas/PopupMenu.set_item_disabled(2,false)


# сдвиг по всем координатам по целеполаганию полёта к объекту
const translacio = 20
const translacio_stat = 300

const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

# вход в станцию
func go_kosmostacioj():
	var q = QueryObject.new()
	# закрываем проект
	#  добавляем запись в связи, что находимся внутри
	var uuid_tasko = ''
	if Global.fenestro_itinero.projekto_itineroj_uuid:
		uuid_tasko = Global.fenestro_itinero.itineroj.front()['uuid_tasko']

	# нужно получить в ответ uuid связи
	Title.id_ligilo = Net.get_current_query_id()
	# Делаем запрос к бэкэнду
	Net.send_json(q.eniri_kosmostacio_ws(
			Global.fenestro_itinero.projekto_itineroj_uuid,
			uuid_tasko, 
			Global.objektoj[index_pos]['uuid'],Title.id_ligilo))

	# добавляем в данные пользователя о станции для последующего выхода
	if !Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
		Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo'] = {'edges':[]}
	Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].append({
		'node': {'posedanto': {'koordinatoX' : Global.objektoj[index_pos]['koordinatoX'],
		'koordinatoY' : Global.objektoj[index_pos]['koordinatoY'],
		'koordinatoZ' : Global.objektoj[index_pos]['koordinatoZ'],
		'kubo': {'objId' : Global.kubo},
		'nomo': {'enhavo': Global.objektoj[index_pos]['nomo']['enhavo']},
		'uuid' : Global.objektoj[index_pos]['uuid']},},})
	Title.CloseWindow()
	Global.direktebla_objekto[Global.realeco-2]['kosmo'] = false
	# вызываем сцену станции
# warning-ignore:return_value_discarded
	get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')


# выбрали позицию в меню
func _on_PopupMenu_index_pressed(index):
# warning-ignore:unused_variable
	var ship = $"../".get_node("ship")
	var objekto
	# вычисляем объект в космосе
	# проходим по всем созданным объектам в космосе и находим нужный по uuid
	for child in $"../".get_children():
		if child.is_in_group('create'):
			if child.uuid == Global.objektoj[index_pos]['uuid']:
				objekto = child
				break
	if index == 2: # если выбран вход в станцию
		go_kosmostacioj()
	elif index == 3: # если выбрана стрельба по объекту
		$"../ship/laser_gun".set_target(objekto)
	elif index == 4: # взять в прицел
		$"../ui_armilo".set_celilo(objekto)
	else: # если выбрано движение к цели или добавление в маршрут
		# вычисляем точку в пространстве, придвинутую на translacio ближе
		# если станция, то дистанция больше
		var celo = Vector3(Global.objektoj[index_pos]['koordinatoX'],
			Global.objektoj[index_pos]['koordinatoY'],
			Global.objektoj[index_pos]['koordinatoZ'])
		
		var dist = 0
		if Global.objektoj[index_pos]['resurso']['objId']==1:
			dist = $"../ship".translation.distance_to(celo) - translacio_stat
		else:
			dist = $"../ship".translation.distance_to(celo) - translacio
		var speed = celo - $"../ship".translation
		celo = $"../ship".translation + speed.normalized() * dist
		if index==0: # отправка корабля к цели
			Global.fenestro_itinero.okazigi_itinero(
				Global.objektoj[index_pos]['uuid'],
				Global.objektoj[index_pos]['nomo']['enhavo'], #'nomo'
				celo.x,
				celo.y,
				celo.z,
				Transform(Basis.IDENTITY, celo),
				dist,
				Net.kategorio_movado,
				false
			)
		elif index==1: # добавление точки в маршрут
			Global.fenestro_itinero.add_itinero(
				'',
				Global.objektoj[index_pos]['uuid'],
				Global.objektoj[index_pos]['nomo']['enhavo'], #'nomo'
				celo.x,
				celo.y,
				celo.z,
				Transform(Basis.IDENTITY, celo),
				dist,
				Net.kategorio_movado,
				-1 #pozicio
			)

