extends Button


const QueryObject = preload("queries.gd")

var one_input = false # первый вход в программу - нужно перезагрузить данные с сервера
var id_nick = 0

func _ready():
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('Ошибка подключения сигнала для обработки входящих данных = ',err)


func save_profile(on_data):
	Global.nickname = on_data['payload']['data']['redaktuUniversoUzanto']['universoUzanto']['retnomo']
	Global.nickname_uuid = on_data['payload']['data']['redaktuUniversoUzanto']['universoUzanto']['uuid']
	# Переходим на сцену с видом на космостанцию
	var err = get_tree().change_scene('res://blokoj/kosmostacioj/Kosmostacio.tscn')
	if err:
		print('Ошибка ',err)

# Вызывается при нажатии кнопки Сохранить
func _pressed():
	# Получаем никнейм из поля ввода
	var nickname = $'../NicknameEdit'.text
	
	# Если никнейм пустой
	if !nickname:
		# Показываем ошибку
		$'../NicknameErrorLabel'.show()
	# Если поле с никнеймом заполнено
	else:
		# Прячем ошибку
		$'../NicknameErrorLabel'.hide()
		
		# Если никнейм не меняли
		if nickname == Global.nickname:
			# Выходим на сцену с видом на космостанцию
			var err = get_tree().change_scene('res://blokoj/kosmostacioj/Kosmostacio.tscn')
			if err:
				print('Ошибка ',err)
			return
		
		# В остальных случаях
		# TODO: подумать нужна ли эта проверка?
		# Если авторизованы
		if Global.status:
			# Объект с данными для запроса (сохранить профиль)
			var q = QueryObject.new()

#			# подключаем сигнал для обработки входящих данных
#			var err = Net.connect("input_data", self, "_on_data")
#			if err:
#				print('Ошибка подключения сигнала для обработки входящих данных = ',err)

			# Здесь будут данные для сохранения (передаются в запрос)
			var profile = {}
			
			# Никнейм
			profile['nickname'] = nickname
			
			# Если никнейм уже был
			if Global.nickname_uuid:
				# заполняем uuid, чтобы запрос обновил поля
				profile['uuid'] = ', uuid: "' + Global.nickname_uuid + '"'
			# В противном случае uuid не нужен
			else:
				profile['uuid'] = ''
				# перезапрашиваем все данные нового пользователя
				one_input = true
			
			id_nick = Net.get_current_query_id()
			# Делаем запрос к бэкэнду
			Net.send_json(q.save_profile_ws(profile, id_nick))


func _on_data():
	if !id_nick:
		return
	var i_data_server = 0
	var q = QueryObject.new()
	for on_data in Net.data_server:
		if int(on_data['id']) == id_nick:
			save_profile(on_data)
			id_nick = 0
			Net.data_server.remove(i_data_server)
			#отключаем перехват сигнала
#			Net.disconnect("input_data", self, "_on_data")
			if one_input:
				Title.fermo_signo()
				Title._on_connection_success()
		i_data_server += 1


