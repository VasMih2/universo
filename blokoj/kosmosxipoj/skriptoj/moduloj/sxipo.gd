extends Node
# модель сбора корабля


const base_cabine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cabine.tscn")
const base_engine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/engine.tscn")
const base_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cargo.tscn")
const canavara_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/VostokU2/Canavara.tscn")
const boozina_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/VostokU2/Boozina.tscn")
const laser = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/laser.tscn")
const laser2 = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/laser2.tscn")
const skif_cabine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/skif/cabine.tscn")
const skif_engins2 = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/skif/engins2.tscn")
const skif_armilo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/skif/guns.tscn")


# функция создания корабля
func create_sxipo(sxipo, objecto):
	if !objecto:
		print('Нет такого объекта на сервере')
		return 404
	var cabine = null
	var cargo = null
	var engine = null
	var moduloj = null
	
	if (objecto['resurso']['objId'] == 3):# это корабль "Vostok U2" "Базовый космический корабль"
		
		var enganes = false # временно - указывает, чтоодин двигатель уже ставили
		# первый цикл добавляет все модули к кораблю
		var max_konektilo = 0 # максимальный номер комплектующей
		#var tek_konektilo = 1 # текущий номер устанавливаемой в пространстве комплектующей
		# на сколько сдвигать по осям
		#var konektilo_x = 0
		#var konektilo_y = 0
		#var konektilo_z = 0
		for modulo in objecto['ligilo']['edges']:
			if max_konektilo < modulo['node']['konektiloPosedanto']:
				max_konektilo = modulo['node']['konektiloPosedanto']
			if modulo['node']['tipo']['objId'] == 1: # связь типа 1 - модуль корабля
				if modulo['node']['ligilo']['resurso']['objId'] == 4: #"Vostok Модуль Кабины" "Базовый модуль кабины кораблей Vostok"
					cabine = base_cabine.instance()
					cabine.uuid = modulo['node']['ligilo']['uuid']
					# x - в право
					cabine.translation.y = 2 #вверх
					cabine.translation.z = 1 # назад
					cabine.rotate_y(deg2rad(90))
					#cabine.get_node("cabine").translation.y = 2 #вверх
					#cabine.get_node("cabine").translation.z = 2 # назад
					#cabine.get_node("cabine").rotate_y(deg2rad(90))
					#sxipo.get_node("CollisionShape").add_child(cabine)
					cabine.integreco = modulo['node']['ligilo']['integreco']
					cabine.objekto_modulo = modulo['node']['ligilo'].duplicate(true)
					sxipo.add_child(cabine)
				#"Vostok Грузовой Модуль" "Базовый грузовой модуль кораблей Vostok"
				# 	Vostok Canavara Грузовой Модуль
				elif modulo['node']['ligilo']['resurso']['objId'] == 5 or \
						modulo['node']['ligilo']['resurso']['objId'] == 19: 
					cargo = base_cargo.instance()
					if modulo['node']['ligilo']['resurso']['objId'] == 19:
						cargo.get_node('cargo').queue_free()
						cargo.add_child(canavara_cargo.instance())
					cargo.uuid = modulo['node']['ligilo']['uuid']
					cargo.translation.y = 2
					cargo.translation.z = 7
					cargo.rotate_y(deg2rad(90))
					# cargo.get_node("Cargo").translation.y = 2
					# cargo.get_node("Cargo").translation.z = 8
					# cargo.get_node("Cargo").rotate_y(deg2rad(90))
					cargo.integreco = modulo['node']['ligilo']['integreco']
					cargo.objekto_modulo = modulo['node']['ligilo'].duplicate(true)
					sxipo.add_child(cargo)
				elif modulo['node']['ligilo']['resurso']['objId'] == 6: #"Vostok Двигатель" "Базовый двигатель кораблей Vostok"
					engine = base_engine.instance()
					engine.uuid = modulo['node']['ligilo']['uuid']
					if enganes:
						engine.translation.x = -2.5
						engine.translation.y = 1
						#engine.get_node("engine R").translation.z = 10
						engine.translation.z = 10
						engine.rotate_y(deg2rad(270))
						engine.rotate_x(deg2rad(180))
					else:
						engine.translation.x = 2.5
						engine.translation.y = 1
						engine.translation.z = 10
						engine.rotate_y(deg2rad(90))
					engine.integreco = modulo['node']['ligilo']['integreco']
					engine.objekto_modulo = modulo['node']['ligilo'].duplicate(true)
					sxipo.add_child(engine)
					enganes = !enganes
				elif modulo['node']['ligilo']['resurso']['objId'] == 11: #Универсальный лазер
					moduloj = laser.instance()
					moduloj.uuid = modulo['node']['ligilo']['uuid']
					moduloj.get_node("Turret").translation.y = 3.5
					moduloj.get_node("Turret").translation.z = 0.1
					moduloj.get_node("Platform").translation.y = 3.5
					moduloj.get_node("Platform").translation.z = 0.1
					moduloj.get_node("laser").translation.y = 4.7
					moduloj.get_node("laser").translation.z = 0.1

					moduloj.get_node("gun_body").translation.y = 4.2
					moduloj.get_node("gun_body").translation.z = -0.4
					moduloj.potenco = modulo['node']['ligilo']['stato']['potenco']
					moduloj.integreco = modulo['node']['ligilo']['integreco']
					moduloj.objekto_modulo = modulo['node']['ligilo'].duplicate(true)
					
					#moduloj.get_node("laser/gun_stem").translation.y = 4.2
					moduloj.get_node("laser/gun_stem").translation.z = -0.6
					#moduloj.get_node("laser/beam/MeshInstance").translation.y = 4.2
					#moduloj.get_node("laser/beam/MeshInstance2").translation.y = 4.2
					#moduloj.get_node("laser/end_point/Particles").translation.y = 4.2
					moduloj.sxipo = sxipo # установили указатель на корабль управления
					sxipo.add_child(moduloj)
					sxipo.armiloj.append(moduloj) # добавили в массив вооружения корабля
	elif (objecto['resurso']['objId'] == 12):# это корабль "Skif EB-1 Poluso" "Базовый малый боевой космический корабль"
		# первый цикл добавляет все модули к кораблю
		var max_konektilo = 0 # максимальный номер комплектующей
		var laser_left = true # первый лазер слева
		for modulo in objecto['ligilo']['edges']:
			if max_konektilo < modulo['node']['konektiloPosedanto']:
				max_konektilo = modulo['node']['konektiloPosedanto']
			if modulo['node']['tipo']['objId'] == 1: # связь типа 1 - модуль корабля
				if modulo['node']['ligilo']['resurso']['objId'] == 13: #"Skif Модуль Кабины"
					cabine = skif_cabine.instance()
					cabine.uuid = modulo['node']['ligilo']['uuid']
					# x - в право
					cabine.translation.y = 2 #вверх
					cabine.translation.z = 3 # назад
					cabine.rotate_y(deg2rad(180))
					cabine.integreco = modulo['node']['ligilo']['integreco']
					cabine.objekto_modulo = modulo['node']['ligilo'].duplicate(true)
					sxipo.add_child(cabine)
				elif modulo['node']['ligilo']['resurso']['objId'] == 14: #"Skif Боевой Модуль"
					cargo = skif_armilo.instance()
					cargo.uuid = modulo['node']['ligilo']['uuid']
					cargo.translation.y = 2
					cargo.translation.z = 3
					cargo.rotate_y(deg2rad(180))
					cargo.integreco = modulo['node']['ligilo']['integreco']
					cargo.objekto_modulo = modulo['node']['ligilo'].duplicate(true)
					sxipo.add_child(cargo)
				elif modulo['node']['ligilo']['resurso']['objId'] == 15: #"Skif Двигательный Модуль"
					engine = skif_engins2.instance()
					engine.uuid = modulo['node']['ligilo']['uuid']
					#engine.translation.x = -2.5
					engine.translation.y = 2
					engine.translation.z = 3
					engine.rotate_y(deg2rad(180))
					engine.integreco = modulo['node']['ligilo']['integreco']
					engine.objekto_modulo = modulo['node']['ligilo'].duplicate(true)
					sxipo.add_child(engine)
				elif modulo['node']['ligilo']['resurso']['objId'] == 16: #Малый Боевой Лазер
					moduloj = laser2.instance()
					moduloj.uuid = modulo['node']['ligilo']['uuid']
					if laser_left:
						moduloj.get_node("Turret").translation.x = -2.5
						moduloj.get_node("Turret").translation.y = 2.0
						moduloj.get_node("Turret").translation.z = 4.1
						moduloj.get_node("Platform").translation.x = -2.5
						moduloj.get_node("Platform").translation.y = 2.0
						moduloj.get_node("Platform").translation.z = 4.1
						moduloj.get_node("laser").translation.x = -2.5
						moduloj.get_node("laser").translation.y = 2.0
						moduloj.get_node("laser").translation.z = 4.1
						moduloj.get_node("gun_body").translation.x = -2.5
						moduloj.get_node("gun_body").translation.y = 4.7
						moduloj.get_node("gun_body").translation.z = 4.6
						# moduloj.get_node("laser/gun_stem").translation.y = 4.2
						moduloj.get_node("laser/gun_stem").translation.z = -0.6
						# moduloj.get_node("laser/beam/MeshInstance").translation.y = 4.2
						# moduloj.get_node("laser/beam/MeshInstance2").translation.y = 4.2
						# moduloj.get_node("laser/end_point/Particles").translation.y = 4.2
						laser_left = !laser_left
					else:
						moduloj.get_node("Turret").translation.x = 2.5
						moduloj.get_node("Turret").translation.y = 2.0
						moduloj.get_node("Turret").translation.z = 4.1
						moduloj.get_node("Platform").translation.x = 2.5
						moduloj.get_node("Platform").translation.y = 2.0
						moduloj.get_node("Platform").translation.z = 4.1
						moduloj.get_node("laser").translation.x = 2.5
						moduloj.get_node("laser").translation.y = 2.0
						moduloj.get_node("laser").translation.z = 4.1
						moduloj.get_node("gun_body").translation.x = 2.5
						moduloj.get_node("gun_body").translation.y = 4.7
						moduloj.get_node("gun_body").translation.z = 4.6
						# moduloj.get_node("laser/gun_stem").translation.y = 4.2
						moduloj.get_node("laser/gun_stem").translation.z = -0.6
						# moduloj.get_node("laser/beam/MeshInstance").translation.y = 4.2
						# moduloj.get_node("laser/beam/MeshInstance2").translation.y = 4.2
						# moduloj.get_node("laser/end_point/Particles").translation.y = 4.2

					if modulo['node']['ligilo']['stato']:
						moduloj.potenco = modulo['node']['ligilo']['stato']['potenco']
					moduloj.integreco = modulo['node']['ligilo']['integreco']
					moduloj.objekto_modulo = modulo['node']['ligilo'].duplicate(true)
					
					moduloj.sxipo = sxipo # установили указатель на корабль управления
					sxipo.add_child(moduloj)
					sxipo.armiloj.append(moduloj) # добавили в массив вооружения корабля


