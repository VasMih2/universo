extends KinematicBody

var max_speed = 500.0
# текущая скорость
var current_speed =0
var acceleration = 1
var target_dir: Vector3 = Vector3.ZERO #направление на эту точку от текущей позиции корабля
var target_rot = null #положение корабля, которое надо принять, чтобы нацелиться на точку.
var speed_rotation = 1
#var middle_mouse_pressed = false
#var docking_rotation

var uuid #uuid активного корабля игрока
#var objekto
var integreco # целостность объекта
var potenco # мощность ресурса

# взятые в прицел, массив объектов, взятых в прицел
var pafado_uuid # uuid проекта ведения огня
var pafado_celo = [] # на какой объект отправили запрос на установку цели
var elektita_celilo = 0 # выбранная цель
var armiloj = [] # ссылка на оружие корабля

var objekto 
var directebla_sxipo = true # признак управляемого корабля

func _ready():
	$CollisionShape.queue_free() # что бы в процессе редактирования не выдавало ошибку есть данная нода


func _physics_process(delta):
	# если категория задачи равна категории движения объекта, тогда двигаемся
	if !(Global.fenestro_itinero.itineroj.empty() or \
			Global.fenestro_itinero.itinero_pause) and \
			(Global.fenestro_itinero.itineroj.front()['kategorio']==Net.kategorio_movado): #Если цель существует, двигаемся
		if !target_rot:
#			print('target_rot = ',Global.fenestro_itinero.itineroj.front()['transform'].origin)
			target_rot = Quat(transform.looking_at(Global.fenestro_itinero.itineroj.front()['transform'].origin,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.

		target_dir = (Global.fenestro_itinero.itineroj.front()['transform'].origin - translation).normalized()
		var distance = translation.distance_to(Global.fenestro_itinero.itineroj.front()['transform'].origin)
		if (len (Global.fenestro_itinero.itineroj)>1) and \
				(Global.fenestro_itinero.itineroj[1]['kategorio']==Net.kategorio_movado):
			# проверяем, если следующая точка является движением
			if distance <1: #Если это не последняя точка, то мы не тормозим, так что завышаем расстояние, чтобы не проскочить эту точку
				dec_route()
				return
			current_speed = lerp(current_speed,max_speed,delta*acceleration)#ускоряемся
			transform.basis = transform.basis.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
		else:
			if distance > max_speed*delta/acceleration:#Тут сомнительная формула от фонаря, вычисляющая примерно откуда надо начинать тормозить корабль.
				current_speed = lerp(current_speed,max_speed,delta*acceleration)#ускоряемся
				var a = Quat(transform.basis)
				var c = a.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
				transform.basis = Basis(c)
#				transform.basis = transform.basis.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
			else: #Если это  последняя точка, то мы тормозим, и задаём минимальное расстояние, чтобы точнее выставить корабль
				current_speed = lerp(current_speed,50,delta*acceleration)#замедляемся
				if Global.fenestro_itinero.itineroj.front()['transform'].basis !=Basis.IDENTITY:
					transform.basis = transform.basis.slerp(Quat(Global.fenestro_itinero.itineroj.front()['transform'].basis),speed_rotation*delta*1.5) #поворачиваем в дефолтное состояние, чтобы сесть
				if distance <0.01:
					if Global.fenestro_itinero.itineroj.front()['transform'].basis !=Basis.IDENTITY:
						transform.basis = Global.fenestro_itinero.itineroj.front()['transform'].basis
					translation = Global.fenestro_itinero.itineroj.front()['transform'].origin
					clear_route()
					return
# warning-ignore:return_value_discarded
		move_and_slide(target_dir*delta*current_speed)
		$"/root/Title/CanvasLayer/UI/indicoj/indicoj".speed = current_speed
		$"/root/Title/CanvasLayer/UI/indicoj/indicoj".FillItemList()
	Title.get_node("CanvasLayer/UI/Objektoj/Window").distance_to(translation)


func rotate_start():# поворачиваем корабль носом по ходу движения
	var front = Transform(Basis.IDENTITY, Vector3(Global.fenestro_itinero.itineroj.front()['koordinatoX'],
		Global.fenestro_itinero.itineroj.front()['koordinatoY'], 
		Global.fenestro_itinero.itineroj.front()['koordinatoZ']))
	target_rot = Quat(transform.looking_at(front.origin,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.


func dec_route():
	target_rot = null
	Global.fenestro_itinero.malmultigi_unua()
	rotate_start()


func clear_route():
	target_rot = null
	# достигли цели, закрываем проект
	Global.fenestro_itinero.fermi_projekto_tasko()


# пришла с сервера задача выстрела
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func pafo(projekto, tasko):
#	=== в стрельбу попали 
	# если стрельба
	if tasko['kategorio']['edges'].front()['node']['objId']==Net.tasko_kategorio_pafo:
		# циклом пробегаемся по armiloj
		# находим оружие и передаём ему цель стрельбы
		for armilo in armiloj:
			# ищем, какое именно оружие стреляло
			if armilo.uuid == tasko['posedanto']['edges'].front()['node']['posedantoObjekto']['uuid']:
				# что делать - стрелять в цель, или прекращать стрелять
				if tasko['statuso']['objId']==Net.statuso_laboranta:
					# находим по uuid цель в списке объектов космоса
					armilo.set_target(Global.fenestro_kosmo.sercxo_objekto_uuid(tasko['objekto']['uuid']))
				else:
					# сбрасываем цель стрельбы в null
					armilo.set_target(null)
	elif tasko['kategorio']['edges'].front()['node']['objId']==Net.tasko_kategorio_celilo:
		print('=== попали в задачу прицеливания projekto=',projekto)
	else:
		print('=== попали не в стрельбу projekto=',projekto)
		print(' ===tasko=',tasko)


# редактировать маршрут
func sxangxi_itinero(projekto, tasko):
	if Global.logs:
		print('=== редактируем маршрут управляемого корабля ===')
	Global.fenestro_itinero.sxangxi_itinero(projekto, tasko)
