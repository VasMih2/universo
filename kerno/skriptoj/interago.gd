extends "res://kerno/fenestroj/tipo_a1.gd"


onready var _button = $VBox/kosmo
onready var _label = $VBox/body_texture/Label


const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")


# ищем в списке объектов объект с конкретным uuid
func search_objekt_uuid(uuid):
	for objekt in Global.objektoj:
		if objekt['uuid']==uuid:
			return objekt['nomo']['enhavo']
	return 'не найден'


func print_button():
	_label.text = ''
	if Global.realeco==1:
		_button.disabled=true
		_button.set_visible(false)
		return
	if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
		_button.text='Войти в станцию'
		_button.disabled=true
		_button.set_visible(false)
	else:
		if Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
			if len(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'])>0:
				var uuid = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['uuid']
				if uuid:
					_label.text = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['nomo']['enhavo']
#					search_objekt_uuid(uuid)# Global.direktebla_objekto[Global.realeco-2]
					_button.text='Выйти в космос'
					if Title.id_ligilo:
						_button.disabled=true
					else:
						_button.disabled=false
					_button.set_visible(true)


func _on_kosmo_pressed():
	if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
		go_kosmostacioj()
	else:
		go_kosmo()
	$VBox.set_visible(false)

# вход в ближайшую станцию (задача - вычислить её в списке объектов)
func go_kosmostacioj():
	pass


func go_kosmo():
	if not Global.direktebla_objekto[Global.realeco-2].has('uuid'):
		print('Нет корабля для этого мира')
		return
	var q = QueryObject.new()
	var del_uuid = '' #есть ли в базе запись о нахождении в станции
	# задаём координаты выхода из станции, согласно координатам станции
	if len(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'])>0:
		Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoX'] + 120
		Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoY'] + 120
		Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoZ'] + 200
		del_uuid = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['uuid']
		Global.kubo = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['kubo']['objId']
		Global.direktebla_objekto[Global.realeco-2]['rotaciaX'] = 0
		Global.direktebla_objekto[Global.realeco-2]['rotaciaY'] = 0
		Global.direktebla_objekto[Global.realeco-2]['rotaciaZ'] = 0
		Global.direktebla_objekto[Global.realeco-2]['kosmo'] = true
		#удаляем в массиве объектов пользователя указатель на станцию
		Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].clear()
	var id = Net.get_current_query_id()
	Net.net_id_clear.push_back(id)
	# Делаем запрос к бэкэнду
	Net.send_json(q.go_objekt_kosmo_ws(
			Global.direktebla_objekto[Global.realeco-2]['uuid'],
			Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
			Global.direktebla_objekto[Global.realeco-2]['koordinatoY'], 
			Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'],
			0,0,0, del_uuid, Global.kubo, id
		))
	Title.CloseWindow()
	var err = get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
	Title.reloadWindow()
	if err:
		print('Ошибка загрузки сцены: ', err)


